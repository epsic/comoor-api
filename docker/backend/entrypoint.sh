#!/bin/bash
echo "Waiting for database to start "
while ! (echo > /dev/tcp/db/5432) ; do echo -n '.'; sleep 1; done;

exec "$@"