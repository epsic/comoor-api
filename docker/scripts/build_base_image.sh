#!/bin/sh
# Build image using cache from container registry
docker build -t $CI_REGISTRY_IMAGE/$IMAGE_NAME:$CI_COMMIT_SHORT_SHA \
             -t $CI_REGISTRY_IMAGE/$IMAGE_NAME \
             -f $DOCKER_FILE .

# Push image into container registry
docker push $CI_REGISTRY_IMAGE/$IMAGE_NAME
