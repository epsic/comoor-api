#!/bin/bash

[[ ! -d "node_modules" ]] && npm install

exec "${@}"
