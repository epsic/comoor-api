export default {
  authenticated(state) {
    return state.loggedIn
  },
  isAdmin(state) {
    return state.isAdmin
  },
  image(state) {
    return state.data.photoURL
  },
  firstName(state) {
    return state.data.displayName.split(' ')[0]
  }
}
