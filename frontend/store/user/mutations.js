export default {
  LOGIN(state, user) {
    state.data = user
    state.isAdmin = user.dbUser.isAdmin
    state.id = user.dbUser.id
    state.loggedIn = true
  },
  LOGOUT(state) {
    state.data = {}
    state.loggedIn = false
    state.isAdmin = false
    state.id = null
  }
}
