export default () => ({
  loggedIn: false,
  isAdmin: false,
  data: {}
})
