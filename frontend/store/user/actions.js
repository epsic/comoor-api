import Firebase from '../../helpers/firebase'

export default {
  watchFirebase({ dispatch }) {
    Firebase.auth.onAuthStateChanged((user) => {
      if (user) {
        dispatch('loginAction', user)
      } else {
        dispatch('logoutAction')
      }
    })
  },
  loginAction({ commit }, user) {
    this.$axios
      .get('User/email', {
        params: {
          email: user.email
        }
      })
      .then((res) => {
        user.dbUser = res.data
        commit('LOGIN', user)
      })
  },
  logoutAction({ commit }) {
    commit('LOGOUT')
  }
}
