import { toggleArrayValue } from '../../helpers/applyFilters'

export default {
  setSearchText: ({ commit }, payload) => {
    commit('SET_SEARCH_TEXT', payload)
  },
  toggleCountry: ({ commit, rootState, dispatch }, payload) => {
    const values = toggleArrayValue(
      rootState.productsFilter.countryFilter,
      payload
    )
    if (values.length) {
      commit('TOGGLE_COUNTRY', values)
    } else {
      dispatch('SET_COUNTRIES_FILTER')
    }
  },
  setPriceRange: ({ commit }, payload) => {
    commit('SET_PRICE_RANGE', payload)
  },
  toggleCategory: ({ commit, rootState, dispatch }, payload) => {
    const values = toggleArrayValue(
      rootState.productsFilter.categoryFilter,
      payload
    )
    if (values.length) {
      commit('TOGGLE_CATEGORY', values)
    } else {
      dispatch('SET_CATEGORIES_FILTER')
    }
  },
  setMinQty: ({ commit }, payload) => {
    commit('SET_MIN_QTY', payload)
  },
  setMinWeight: ({ commit }, payload) => {
    commit('SET_MIN_WEIGHT', payload)
  },
  toggleOdor: ({ commit, rootState, dispatch }, payload) => {
    const values = toggleArrayValue(
      rootState.productsFilter.odorFilter,
      payload
    )
    if (values.length) {
      commit('TOGGLE_ODOR', values)
    } else {
      dispatch('SET_ODORS_FILTER')
    }
  },
  toggleAnimalOrigin: ({ commit, rootState, dispatch }, payload) => {
    const values = toggleArrayValue(
      rootState.productsFilter.animalFilter,
      payload
    )
    if (values.length) {
      commit('TOGGLE_ANIMAL_ORIGIN', values)
    } else {
      dispatch('SET_ANIMALS_FILTER')
    }
  },
  SET_ALL_FILTERS: ({ dispatch }) => {
    dispatch('SET_COUNTRIES_FILTER')
    dispatch('SET_CATEGORIES_FILTER')
    dispatch('SET_ANIMALS_FILTER')
    dispatch('SET_ODORS_FILTER')
    dispatch('setSearchText', '')
    dispatch('setPriceRange', [0, 150])
    dispatch('setMinQty', 0)
    dispatch('setMinWeight', 0)
  },
  SET_COUNTRIES_FILTER: ({ commit, rootState }) => {
    const countriesTypes = rootState.availableTypes.types.countries.map(
      (country) => country.id
    )
    commit('SET_COUNTRIES_FILTER', countriesTypes)
  },
  SET_CATEGORIES_FILTER: ({ commit, rootState }) => {
    const categoryTypes = rootState.availableTypes.types.categories.map(
      (cat) => cat.id
    )
    commit('SET_CATEGORIES_FILTER', categoryTypes)
  },
  SET_ANIMALS_FILTER: ({ commit, rootState }) => {
    const animalsTypes = rootState.availableTypes.types.animals.map(
      (animal) => animal.id
    )
    commit('SET_ANIMALS_FILTER', animalsTypes)
  },
  SET_ODORS_FILTER: ({ commit, rootState }) => {
    const odorsType = rootState.availableTypes.types.odors.map(
      (odor) => odor.id
    )
    commit('SET_ODORS_FILTER', odorsType)
  }
}
