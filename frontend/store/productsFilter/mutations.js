import Vue from 'vue'
export default {
  SET_SEARCH_TEXT(state, payload) {
    state.searchText = payload
  },
  TOGGLE_COUNTRY(state, payload) {
    Vue.set(state, 'countryFilter', payload)
  },
  SET_PRICE_RANGE(state, payload) {
    state.priceRange = payload
  },
  TOGGLE_CATEGORY(state, payload) {
    Vue.set(state, 'categoryFilter', payload)
  },
  SET_MIN_QTY(state, payload) {
    state.minQty = payload
  },
  SET_MIN_WEIGHT(state, payload) {
    state.minWeight = payload
  },
  TOGGLE_ODOR(state, payload) {
    Vue.set(state, 'odorFilter', payload)
  },
  TOGGLE_ANIMAL_ORIGIN(state, payload) {
    Vue.set(state, 'animalFilter', payload)
  },
  SET_COUNTRIES_FILTER: (state, payload) => {
    Vue.set(state, 'countryFilter', payload)
  },
  SET_CATEGORIES_FILTER: (state, payload) => {
    Vue.set(state, 'categoryFilter', payload)
  },
  SET_ANIMALS_FILTER: (state, payload) => {
    Vue.set(state, 'animalFilter', payload)
  },
  SET_ODORS_FILTER: (state, payload) => {
    Vue.set(state, 'odorFilter', payload)
  }
}
