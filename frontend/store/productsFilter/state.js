export default () => ({
  searchText: '',
  countryFilter: [],
  minQty: 5,
  minWeight: 0,
  priceRange: [0, 150],
  categoryFilter: [],
  odorFilter: [],
  animalFilter: []
})
