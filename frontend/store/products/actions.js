import axios from 'axios'
import _ from 'lodash'
export default {
  GET_ALL_STORAGES({ dispatch }) {
    return new Promise((resolve, reject) => {
      this.$axios
        .get('stock')
        .then((res) => {
          dispatch('FILTERS_PRODUCTS_STORAGE', res.data)
          resolve(res)
        })
        .catch((err) => reject(err))
    })
  },
  SSR_GET_ALL_STORAGES({ dispatch }) {
    return new Promise((resolve, reject) => {
      axios
        .get('https://api-saycheese.herokuapp.com/api/stock')
        .then((res) => {
          dispatch('FILTERS_PRODUCTS_STORAGE', res.data)
          resolve(res)
        })
        .catch((err) => reject(err))
    })
  },
  FILTERS_PRODUCTS_STORAGE({ commit }, payload) {
    const groupedPayload = _.mapValues(_.groupBy(payload, 'productId'))
    for (const item in groupedPayload) {
      commit('SET_PRODUCT_STORAGE', {
        data: groupedPayload[item],
        id: groupedPayload[item][0].productId || null
      })
    }
  },
  ADD_STORAGE({ commit }, payload) {
    return new Promise((resolve, reject) => {
      const data = {
        weight: payload.weight,
        quantity: payload.quantity,
        productId: payload.productId
      }
      this.$axios
        .post('stock', data)
        .then((res) => {
          commit('ADD_STORAGE', res.data)
          resolve('Stock added successfully')
        })
        .catch((err) => reject(err))
    })
  },
  UPDATE_STORAGE({ commit }, payload) {
    return new Promise((resolve, reject) => {
      const data = {
        id: payload.id,
        weight: payload.weight,
        quantity: payload.quantity,
        productId: payload.productId
      }
      this.$axios
        .put('stock/' + payload.id, data)
        .then((res) => {
          commit('UPDATE_STORAGE', data)
          resolve('Stock edited successfully')
        })
        .catch((err) => reject(err))
    })
  },
  DELETE_STORAGE({ commit }, payload) {
    return new Promise((resolve, reject) => {
      this.$axios
        .delete('stock/' + payload.id)
        .then((res) => {
          commit('DELETE_STORAGE', {
            id: payload.id,
            productId: payload.productId
          })
          resolve('Stock removed successfully')
        })
        .catch((err) => reject(err))
    })
  },
  DELETE_PRODUCT({ commit }, payload) {
    return new Promise((resolve, reject) => {
      this.$axios
        .delete('product/' + payload)
        .then((res) => {
          commit('DELETE_PRODUCT', { id: payload })
          resolve('Product removed successfully')
        })
        .catch((err) => reject(err))
    })
  },

  ADD_PRODUCT({ dispatch, rootState }, payload) {
    return new Promise((resolve, reject) => {
      console.log('BEFORE AXIOS', this.$axios)
      console.log('typeof paylaod', typeof payload)
      console.log(' paylaod', payload)
      this.$axios
        .post('product', { ...payload, ownerId: rootState.user.id })
        .then((res) => {
          const { data } = res
          data.mutation = 'ADD_PRODUCT'
          dispatch('MERGE_PRODUCT', data).then((response) => {
            resolve({
              text:
                'Product ' +
                response +
                " successfully\n don't forget to add a storage for this product, so it will be shown in the products page.",
              id: res.data.id
            })
          })
        })
    })
  },
  MERGE_PRODUCT({ commit }, payload) {
    const mutation = payload.mutation
    delete payload.mutation
    const actionMessage = mutation === 'ADD_PRODUCT' ? 'added' : 'edited'
    return new Promise((resolve, reject) => {
      this.$axios
        .get('product/' + payload.id)
        .then((res) => {
          const data = { ...payload, ...res.data }
          commit(mutation, data)
          resolve(actionMessage)
        })
        .catch((err) => reject(err))
    })
  },
  EDIT_PRODUCT({ dispatch }, payload) {
    payload.mutation = 'EDIT_PRODUCT'
    return new Promise((resolve, reject) => {
      this.$axios
        .put('product/' + payload.id, payload)
        .then((res) => {
          dispatch('MERGE_PRODUCT', payload).then((response) => {
            resolve(response)
          })

          resolve('Product edited successfully')
        })
        .catch((err) => reject(err))
    })
  }
}
