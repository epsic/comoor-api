import { helperFilterProducts } from '../../helpers/applyFilters'

export default {
  allProductsList: (state, rootState, getters) => () => {
    return state.list
  },
  filteredProducts: (state, rootState, getters) => () => {
    const filters = getters.productsFilter
    const filteredProducts = helperFilterProducts(state.list, filters)
    return filteredProducts
  },
  lastProductsList: (state) => (number) => {
    const prods = state.list
    const lastProducts = prods
      .slice(0, number)
      .sort((a, b) => b.dateAdded - a.dateAdded)
    return lastProducts
  },
  getSingleProductStorage: (state) => (id) => {
    const product = state.list.filter((prod) => prod.id === id)[0]
    return product ? product.storage : []
  },
  getSingleStorage: (state) => (productId, storageId) => {
    const product = state.list.filter((prod) => prod.id === productId)[0]
    if (product.storage) {
      const storage = product.storage.filter(
        (store) => store.id === storageId
      )[0]
      return storage.quantity
    }
  },
  getMaxPrice: (state) => () => {
    const pricesArray = state.list.map((item) => item.price)
    return Math.max.apply(null, pricesArray)
  },
  getProductPrice: (state) => (productId) => {
    const product = state.list.filter((prod) => prod.id === productId)[0]
    if (product) {
      return product.price
    }
    return ''
  },
  getProductName: (state) => (productId) => {
    const product = state.list.filter((prod) => prod.id === productId)[0]
    if (product) {
      return product.name
    } else {
      return 'Loading...'
    }
  },
  getStorageQty: (state) => (productId, weightValue) => {
    const product = state.list.filter((item) => item.id === productId)[0]
    if (product) {
      const storageId = product.storage.filter(
        (stor) => stor.weight === weightValue
      )[0]
      return storageId.quantity
    }
  }
}
