export default {
  SET_PRODUCTS(state, products) {
    state.list = products
  },
  SET_PRODUCT_STORAGE(state, payload) {
    if (payload.id) {
      const product = state.list.filter(
        (product) => product.id === payload.id
      )[0]
      product.storage = payload.data
    }
  },
  ADD_STORAGE(state, payload) {
    const updatedProduct = state.list.filter(
      (product) => product.id === payload.productId
    )[0]
    if (!updatedProduct.storage) {
      updatedProduct.storage = [payload]
    } else {
      updatedProduct.storage.push(payload)
    }
  },
  UPDATE_STORAGE(state, payload) {
    const product = state.list.filter(
      (item) => item.id === payload.productId
    )[0]
    if (product) {
      const updatedStorage = product.storage.map((stock) => {
        if (stock.id === payload.id) {
          return payload
        } else {
          return stock
        }
      })
      product.storage = updatedStorage
    }
  },
  DELETE_STORAGE(state, payload) {
    const product = state.list.filter(
      (item) => item.id === payload.productId
    )[0]
    if (product) {
      const updatedStorage = product.storage.filter(
        (stock) => stock.id !== payload.id
      )
      product.storage = updatedStorage
    }
  },
  DELETE_PRODUCT(state, payload) {
    const products = state.list.filter((item) => item.id !== payload.id)
    state.list = products
  },
  ADD_PRODUCT(state, payload) {
    state.list.unshift(payload)
  },
  EDIT_PRODUCT(state, payload) {
    const updateProductList = state.list.map((product) => {
      if (product.id === payload.id) {
        return { ...product, ...payload }
      } else {
        return product
      }
    })
    state.list = updateProductList
  }
}
