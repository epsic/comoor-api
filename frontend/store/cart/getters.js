export default {
  getCartTotalPrice: (state, context, rootState, getters) => () => {
    const productsId = []
    state.cart.map((cartItem) => {
      if (!productsId.includes(cartItem.productId)) {
        productsId.push(cartItem.productId)
      }
    })
    const getProductPrice = getters['products/getProductPrice']
    const products = productsId.map((prodPrice) => {
      const price = getProductPrice(prodPrice)
      return { productId: prodPrice, price }
    })

    let globalPrice = 0
    state.cart.forEach((item) => {
      const itemRefs = products.find(
        (prod) => prod.productId === item.productId
      )
      globalPrice = globalPrice + item.quantity * itemRefs.price
    })
    return parseFloat(globalPrice).toFixed(2)
  },
  getCartQuantity: (state) => (productId, weightValue) => {
    const cartItem = state.cart.filter(
      (item) => item.productId === productId && item.weight === weightValue
    )[0]
    return cartItem.quantity
  }
}
