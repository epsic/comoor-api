export default {
  ADD_ITEM_TO_CART(state, payload) {
    const cartItem = state.cart.filter(
      (item) => item.productId === payload.productId
    )[0]
    if (cartItem) {
      if (cartItem.weight === payload.weight) {
        cartItem.quantity += 1
      } else {
        state.cart.unshift(payload)
      }
    } else {
      state.cart.unshift(payload)
    }
  },
  DELETE_ITEM_TO_CART(state, payload) {
    const itemIndex = state.cart.findIndex((item) => item.id === payload.id)
    state.cart.splice(itemIndex, 1)
  },
  DECREMENT_QUANTITY(state, payload) {
    const cartItem = state.cart.filter((item) => item.id === payload.id)[0]
    cartItem.quantity -= 1
  },
  INCREMENT_QUANTITY(state, payload) {
    const cartItem = state.cart.filter((item) => item.id === payload.id)[0]
    if (payload.quantity) {
      cartItem.quantity += payload.quantity
    } else {
      cartItem.quantity += 1
    }
  },
  SET_USER_CART(state, payload) {
    state.cart = payload
  }
}
