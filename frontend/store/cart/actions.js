export default {
  addItemToCart({ commit, rootState }, payload) {
    return new Promise((resolve, reject) => {
      this.$axios
        .post('cart', { ...payload, userId: rootState.user.id })
        .then((res) => {
          commit('ADD_ITEM_TO_CART', { ...payload, id: res.data.id })
          resolve('Item added in your cart')
        })
        .catch((e) => reject(e))
    })
  },
  deleteItemToCart({ commit }, payload) {
    return new Promise((resolve, reject) => {
      this.$axios
        .delete('cart/' + payload.id)
        .then((res) => {
          commit('DELETE_ITEM_TO_CART', { id: payload.id })
        })
        .catch((e) => reject(e))
    })
  },
  incrementQuantity({ commit }, payload) {
    commit('INCREMENT_QUANTITY', payload)
  },
  decrementQuantity({ commit }, payload) {
    commit('DECREMENT_QUANTITY', payload)
  },
  setUserCart({ commit, rootState }) {
    return new Promise((resolve, reject) => {
      this.$axios
        .get('cart', { params: { userId: rootState.user.id } })
        .then((res) => {
          commit('SET_USER_CART', res.data)
        })
        .catch((e) => reject(e))
    })
  }
}
