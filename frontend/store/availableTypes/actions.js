import {
  findArray,
  removeUndefinedOrNullValues
} from '../../helpers/availableTypes'

export default {
  UPDATE_TYPE({ commit }, payload) {
    let data = {
      id: payload.id,
      name: payload.name ? payload.name : null,
      animal: payload.animal ? payload.animal : null
    }
    data = removeUndefinedOrNullValues(data)
    return new Promise((resolve, reject) =>
      this.$axios
        .put(payload.label, data)
        .then((res) => {
          const targetMutation = findArray(payload.target)
          commit('UPDATE_' + targetMutation, data)
          resolve(res)
        })
        .catch((e) => reject(e))
    )
  },
  UPDATE_COUNTRY_TYPE({ commit }, payload) {
    let data = {
      id: payload.id,
      country: payload.country,
      city: payload.city,
      appellation: payload.appellation || null
    }
    data = removeUndefinedOrNullValues(data)
    return new Promise((resolve, reject) =>
      this.$axios
        .put('/origin/' + payload.id, data)
        .then((res) => {
          commit('UPDATE_COUNTRIES_TYPE', data)
          resolve(res)
        })
        .catch((e) => reject(e))
    )
  },
  DELETE_TYPE({ commit }, payload) {
    return new Promise((resolve, reject) =>
      this.$axios
        .delete('' + payload.label)
        .then((res) => {
          const targetMutation = findArray(payload.target)
          commit('DELETE_' + targetMutation, payload.id)
          resolve(res)
        })
        .catch((e) => reject(e))
    )
  },
  ADD_TYPE({ commit }, payload) {
    let data = {
      name: payload.name,
      animal: payload.animal || null
    }
    data = removeUndefinedOrNullValues(data)
    return new Promise((resolve, reject) =>
      this.$axios
        .post('' + payload.label, data)
        .then((res) => {
          const targetMutation = findArray(payload.target)
          commit('ADD_' + targetMutation, res.data)
          resolve(res)
        })
        .catch((e) => reject(e))
    )
  },
  ADD_COUNTRY_TYPE({ commit }, payload) {
    let data = payload
    data = removeUndefinedOrNullValues(data)
    return new Promise((resolve, reject) =>
      this.$axios
        .post('origin', data)
        .then((res) => {
          commit('ADD_COUNTRIES_TYPES', res.data)
          resolve(res)
        })
        .catch((e) => reject(e))
    )
  },
  GET_ALL_TYPES: ({ dispatch }) => {
    dispatch('GET_CATEGORIES_TYPES')
    dispatch('GET_ODOR_TYPES')
    dispatch('GET_COUNTRIES_TYPES')
    dispatch('GET_ANIMALS_TYPES')
  },
  async GET_CATEGORIES_TYPES({ commit }) {
    const { data } = await this.$axios.get(`consistency`)
    commit('SET_CATEGORIES_TYPES', data)
  },
  async GET_ODOR_TYPES({ commit }) {
    const { data } = await this.$axios.get(`smell`)
    commit('SET_ODOR_TYPES', data)
  },
  async GET_COUNTRIES_TYPES({ commit }) {
    const { data } = await this.$axios.get(`origin`)
    commit('SET_COUNTRIES_TYPES', data)
  },
  async GET_ANIMALS_TYPES({ commit }) {
    const { data } = await this.$axios.get(`animalOrigin`)
    commit('SET_ANIMALS_TYPES', data)
  },
  FIND_TYPE_TO_UPDATE({ commit }, payload) {}
}
