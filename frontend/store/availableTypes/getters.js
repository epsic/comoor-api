export default {
  getAllOdors: (state) => () => {
    return state.types.odors
  },
  getAllCategories: (state) => () => {
    return state.types.categories
  },
  getAllCountries: (state) => () => {
    return state.types.countries
  },
  getAllAnimals: (state) => () => {
    return state.types.animals
  },
  getAllTypes: (state) => () => {
    return state.types
  }
}
