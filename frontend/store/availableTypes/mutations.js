export default {
  SET_COUNTRIES_TYPES(state, types) {
    state.types.countries = types
  },
  SET_CATEGORIES_TYPES(state, types) {
    state.types.categories = types
  },
  SET_ODOR_TYPES(state, types) {
    state.types.odors = types
  },
  SET_ANIMALS_TYPES(state, types) {
    state.types.animals = types
  },
  UPDATE_ODORS_TYPES(state, payload) {
    state.types.odors = state.types.odors.map((type) => {
      if (type.id === payload.id) {
        return { ...type, name: payload.name }
      } else {
        return type
      }
    })
  },
  UPDATE_CATEGORIES_TYPES(state, payload) {
    state.types.categories = state.types.categories.map((type) => {
      if (type.id === payload.id) {
        return { ...type, name: payload.name }
      } else {
        return type
      }
    })
  },
  UPDATE_ANIMALS_TYPES(state, payload) {
    state.types.animals = state.types.animals.map((type) => {
      if (type.id === payload.id) {
        return { ...type, animal: payload.animal }
      } else {
        return type
      }
    })
  },
  UPDATE_COUNTRIES_TYPE(state, payload) {
    const { country, city, appellation } = payload
    state.types.countries = state.types.countries.map((type) => {
      if (type.id === payload.id) {
        return { ...type, country, city, appellation }
      } else {
        return type
      }
    })
  },
  DELETE_ODORS_TYPES(state, payload) {
    state.types.odors = state.types.odors.filter((item) => item.id !== payload)
  },
  DELETE_ANIMALS_TYPES(state, payload) {
    state.types.animals = state.types.animals.filter(
      (item) => item.id !== payload
    )
  },
  DELETE_CATEGORIES_TYPES(state, payload) {
    state.types.categories = state.types.categories.filter(
      (item) => item.id !== payload
    )
  },
  DELETE_COUNTRIES_TYPES(state, payload) {
    state.types.countries = state.types.countries.filter(
      (item) => item.id !== payload
    )
  },
  ADD_ODORS_TYPES(state, payload) {
    state.types.odors.push(payload)
  },
  ADD_CATEGORIES_TYPES(state, payload) {
    state.types.categories.push(payload)
  },
  ADD_ANIMALS_TYPES(state, payload) {
    state.types.animals.push(payload)
  },
  ADD_COUNTRIES_TYPES(state, payload) {
    state.types.countries.push(payload)
  }
}
