import { notification } from 'ant-design-vue/lib'

export default function({ $axios }) {
  $axios.onError((error) => {
    notification.error({
      message: error.response.data.title || 'Error',
      description: error.response.data.detail || 'An error occured',
      duration: 4.5
    })
  })
}
