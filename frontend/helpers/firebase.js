import * as firebase from 'firebase/app'
import 'firebase/auth'

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyBIuOEj8e6fONidPQS5cuPFc4gnLylPZ6g',
  authDomain: 'say-cheese-9a28f.firebaseapp.com',
  databaseURL: 'https://say-cheese-9a28f.firebaseio.com',
  projectId: 'say-cheese-9a28f',
  storageBucket: 'say-cheese-9a28f.appspot.com',
  messagingSenderId: '816126828337',
  appId: '1:816126828337:web:e4d825262a26d376bc9a10'
}
// Initialize Firebase
try {
  firebase.initializeApp(firebaseConfig)
} catch (e) {
  //
}
export default {
  auth: firebase.auth(),

  login() {
    const provider = new firebase.auth.GoogleAuthProvider()
    return firebase
      .auth()
      .signInWithPopup(provider)
      .catch(function(error) {
        const errorCode = error.code
        const errorMessage = error.message
        const email = error.email
        const credential = error.credential
        console.log(errorCode, errorMessage, email, credential)
      })
  },

  logout() {
    firebase
      .auth()
      .signOut()
      .then(function() {})
      .catch(function(error) {
        console.log(error)
      })
  }
}
