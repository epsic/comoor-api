export const helperFilterProducts = (productsArray, filters) => {
  const filteredProducts = productsArray.filter((product) => {
    const {
      price,
      consistencyId,
      smellId,
      originId,
      description,
      name,
      animalOriginId,
      storage
    } = product
    const {
      searchText,
      countryFilter,
      categoryFilter,
      priceRange,
      odorFilter,
      animalFilter,
      minQty,
      minWeight
    } = filters

    let priceMatch = false
    let qtyMatch = false
    let weightMatch = false
    let textMatch

    if (price >= priceRange[0] && price <= priceRange[1]) {
      priceMatch = true
    }

    if (storage) {
      const quantityArray = storage.map((store) => store.quantity)
      const weightArray = storage.map((store) => store.weight)
      let hasQty = false
      let hasWeight = false

      quantityArray.forEach((qty) => {
        if (qty >= minQty) hasQty = true
      })
      weightArray.forEach((wgt) => {
        if (wgt >= minWeight) hasWeight = true
      })
      if (hasQty || minQty === 1) {
        qtyMatch = true
      }
      if (hasWeight || minWeight === 10) {
        weightMatch = true
      }
    }

    const animalMatch = animalFilter.includes(animalOriginId)
    const categoryMatch = categoryFilter.includes(consistencyId)
    const odorMatch = odorFilter.includes(smellId)
    const countryMatch = countryFilter.includes(originId)

    if (searchText) {
      textMatch =
        name.toLowerCase().includes(searchText.toLowerCase()) ||
        description.toLowerCase().includes(searchText.toLowerCase())
    } else {
      textMatch = true
    }

    return (
      priceMatch &&
      categoryMatch &&
      odorMatch &&
      qtyMatch &&
      weightMatch &&
      textMatch &&
      animalMatch &&
      countryMatch
    )
  })
  return filteredProducts
}

export const toggleArrayValue = (arr, value) => {
  if (arr.includes(value)) {
    return arr.filter((item) => item !== value)
  } else {
    return [...arr, value]
  }
}
