import axios from 'axios'

export const initStore = async (store) => {
  const apiUrl = 'https://api-saycheese.herokuapp.com/api/'
  if (!store.state.products.list.length) {
    const { data } = await axios.get(apiUrl + 'product')
    const countriesTypes = await axios.get(apiUrl + 'origin')
    const odorsTypes = await axios.get(apiUrl + 'smell')
    const categoriesTypes = await axios.get(apiUrl + 'consistency')
    const animalsTypes = await axios.get(apiUrl + 'animalOrigin')
    store.commit('products/SET_PRODUCTS', data)
    store.commit('availableTypes/SET_ANIMALS_TYPES', animalsTypes.data)
    store.commit('availableTypes/SET_CATEGORIES_TYPES', categoriesTypes.data)
    store.commit('availableTypes/SET_COUNTRIES_TYPES', countriesTypes.data)
    store.commit('availableTypes/SET_ODOR_TYPES', odorsTypes.data)
    await store.dispatch('products/SSR_GET_ALL_STORAGES', null, {
      root: true
    })
  }
}
