import _ from 'lodash'
export const findArray = (target) => {
  switch (target) {
    case 'odors':
      return 'ODORS_TYPES'
    case 'animals':
      return 'ANIMALS_TYPES'
    case 'categories':
      return 'CATEGORIES_TYPES'
    case 'countries':
      return 'COUNTRIES_TYPES'
  }
}
export const generateUrl = (id, target) => {
  switch (target) {
    case 'odors':
      if (id) return `smell/${id}`
      if (!id) return `smell/`
      break
    case 'animals':
      if (id) return `animalOrigin/${id}`
      if (!id) return `animalOrigin/`
      break
    case 'categories':
      if (id) return `consistency/${id}`
      if (!id) return `consistency/`
      break
    case 'countries':
      if (id) return `origin/${id}`
      if (!id) return `origin/`
      break
  }
}

export const removeUndefinedOrNullValues = (data) => {
  return _.omitBy(data, _.isNil)
}

export const isArrayEqual = (x, y) => _.isEmpty(_.xorWith(x, y, _.isEqual))

export const findItemInArray = (id, arr) => {
  return arr.filter((item) => item.id === id)
}
