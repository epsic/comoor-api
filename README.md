# Saycheese 
E-commerce of cheeses

### Project progress
 - [Backlog](https://gitlab.com/epsic/comoor-api/-/boards/1512647)
 - [Support](https://gitlab.com/epsic/comoor-api/-/boards/1512601?&label_name[]=Bug)
 - [By techno](https://gitlab.com/epsic/comoor-api/-/boards/1512622)
 - [Finished](https://gitlab.com/epsic/comoor-api/issues?scope=all&utf8=%E2%9C%93&state=closed)

## Installation

### 1. Install [Pontsun](https://github.com/liip/pontsun) 

### 2. Create and adapt the .env file
```sh
$ cp .env.example .env
```
Adapt the `.env` file as needed.

### 3. Run the containers:
From the root of the project run the following commands
```sh
$ docker-compose pull
$ docker-compose up --build
```

### 4 Go to the application
 - [Frontend](https://saycheese.docker.test/)  
 - [Backend](/https://api.saycheese.docker.test/index.html)

## Docker Containers
Saycheese is composed of 4 container: 
  - **db**: The database container
  - **api**: Is the backend container, it is in charge of the 
  api. Composed with a swagger frontend and dot.net web api.
  contains the code inside the `Comoor` folder
  - **tools**: this container contains the same image than api, 
  it is mainly used to run command linked to the database or nuget
  - **frontend**: It runs a nuxt server, and contains the code inside the `frontend` folder

## Testing
Use `_testOutputHelper.WriteLine(<value>);` to see output text when running tests

## Database Migration
To update your database to the lastest migration please run the following command:

```bash
dcr tools dotnet ef database update
```