using System.ComponentModel.DataAnnotations;

namespace Comoor.Models
{
  public class Smell
  {
    public int Id { get; set; }
    [Required] public string Name { get; set; }
  }
}
