using System.ComponentModel.DataAnnotations;

namespace Comoor.Models
{
    public class Cart
    {
        public int Id { get; set; }
        [Required] public int UserId { get; set; }
        public User User { get; set; }
        [Required] public int ProductId { get; set; }
        public Product Product { get; set; }
        [Required] public float Quantity { get; set; }
        [Required] public int Weight { get; set; }
    }
}
