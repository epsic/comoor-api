using System.ComponentModel.DataAnnotations;

namespace Comoor.Models
{
  public class User
  {
    public int Id { get; set; }
    public bool IsAdmin { get; set; }  
    [Required] public string Email { get; set; }
  }
}
