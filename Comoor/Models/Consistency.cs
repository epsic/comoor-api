using System.ComponentModel.DataAnnotations;

namespace Comoor.Models
{
  public class Consistency
  {
    public int Id { get; set; }
    [Required] public string Name { get; set; }
  }
}
