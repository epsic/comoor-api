using System.ComponentModel.DataAnnotations;

namespace Comoor.Models
{
  public class AnimalOrigin
  {
    public int Id { get; set; }
    [Required] public string Animal { get; set; }
  }
}
