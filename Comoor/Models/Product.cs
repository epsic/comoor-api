using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

using System;
using System.ComponentModel.DataAnnotations;

namespace Comoor.Models
{
    public class Product
    {
        public int Id { get; set; }
        [Required] public string Name { get; set; }

        [Required] public int OriginId { get; set; }
        public Origin Origin { get; set; }

        [Required] public int AnimalOriginId { get; set; }
        public AnimalOrigin AnimalOrigin { get; set; }

        [Required] public int ConsistencyId { get; set; }
        public Consistency Consistency { get; set; }
        
        [Required] public double Price { get; set; }
        
        
        # nullable enable
        public string? ImageUrl { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int? Maturation { get; set; }
        public string? Description { get; set; }

        public int? SmellId { get; set; }
        public Smell? Smell { get; set; }
        # nullable restore
    }
}
