using System.ComponentModel.DataAnnotations;

namespace Comoor.Models
{
  public class Origin
  {
    public int Id { get; set; }
    [Required] public string Country { get; set; }
    [Required] public string City { get; set; }
    
    #nullable enable
    public string? Appellation { get; set; }
    #nullable restore
  }
}

