using System.ComponentModel.DataAnnotations;

namespace Comoor.Models
{
  public class Stock
  {
    public int Id { get; set; }
    [Required] public float Quantity { get; set; }
    [Required] public int Weight { get; set; }
    [Required] public int ProductId { get; set; }
    public Product Product { get; set; }
  }
}
