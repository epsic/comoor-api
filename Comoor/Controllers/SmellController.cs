using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Comoor.Data;
using Comoor.Models;

namespace Comoor.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SmellController : ControllerBase
    {
        private readonly ComoorContext _context;

        public SmellController(ComoorContext context)
        {
            _context = context;
        }

        // GET: api/Smell
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Smell>>> GetSmells()
        {
            return await _context.Smells.ToListAsync();
        }

        // GET: api/Smell/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Smell>> GetSmell(int id)
        {
            var smell = await _context.Smells.FindAsync(id);

            if (smell == null)
            {
                return NotFound();
            }

            return smell;
        }

        // PUT: api/Smell/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSmell(int id, Smell smell)
        {
            if (id != smell.Id)
            {
                return BadRequest();
            }

            _context.Entry(smell).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SmellExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Smell
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Smell>> PostSmell(Smell smell)
        {
            bool nameIsTaken = _context.Smells.Any(s => s.Name == smell.Name);

            if (nameIsTaken)
            {
                return ValidationProblem("Name already taken"); 
            }
            _context.Smells.Add(smell);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSmell", new { id = smell.Id }, smell);
        }

        // DELETE: api/Smell/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Smell>> DeleteSmell(int id)
        {
            var smell = await _context.Smells.FindAsync(id);
            if (smell == null)
            {
                return NotFound();
            }

            _context.Smells.Remove(smell);
            await _context.SaveChangesAsync();

            return smell;
        }

        private bool SmellExists(int id)
        {
            return _context.Smells.Any(e => e.Id == id);
        }
    }
}
