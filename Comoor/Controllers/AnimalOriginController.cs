using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Comoor.Data;
using Comoor.Models;

namespace Comoor.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnimalOriginController : ControllerBase
    {
        private readonly ComoorContext _context;

        public AnimalOriginController(ComoorContext context)
        {
            _context = context;
        }

        // GET: api/AnimalOrigin
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AnimalOrigin>>> GetAnimalOrigins()
        {
            return await _context.AnimalOrigins.ToListAsync();
        }

        // GET: api/AnimalOrigin/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AnimalOrigin>> GetAnimalOrigin(int id)
        {
            var animalOrigin = await _context.AnimalOrigins.FindAsync(id);

            if (animalOrigin == null)
            {
                return NotFound();
            }

            return animalOrigin;
        }

        // PUT: api/AnimalOrigin/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAnimalOrigin(int id, AnimalOrigin animalOrigin)
        {
            try
            {
                Validate(animalOrigin);
            }
            catch (Exception e)
            {
                return ValidationProblem(e.Message);
            }
            if (id != animalOrigin.Id)
            {
                return BadRequest();
            }

            _context.Entry(animalOrigin).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AnimalOriginExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/AnimalOrigin
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<AnimalOrigin>> PostAnimalOrigin(AnimalOrigin animalOrigin)
        {
            try
            {
                Validate(animalOrigin);
            }
            catch (Exception e)
            {
                return ValidationProblem(e.Message);
            }

            _context.AnimalOrigins.Add(animalOrigin);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAnimalOrigin", new { id = animalOrigin.Id }, animalOrigin);
        }

        private void Validate(AnimalOrigin animalOrigin)
        {
            bool existAlready = _context.AnimalOrigins.Any(a =>a.Animal == animalOrigin.Animal);

            if (existAlready)
            {
                throw new Exception("Animal exist already");
            }
        }

        // DELETE: api/AnimalOrigin/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<AnimalOrigin>> DeleteAnimalOrigin(int id)
        {
            var animalOrigin = await _context.AnimalOrigins.FindAsync(id);
            if (animalOrigin == null)
            {
                return NotFound();
            }

            _context.AnimalOrigins.Remove(animalOrigin);
            await _context.SaveChangesAsync();

            return animalOrigin;
        }

        private bool AnimalOriginExists(int id)
        {
            return _context.AnimalOrigins.Any(e => e.Id == id);
        }
    }
}
