using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Comoor.Data;
using Comoor.Models;

namespace Comoor.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConsistencyController : ControllerBase
    {
        private readonly ComoorContext _context;

        public ConsistencyController(ComoorContext context)
        {
            _context = context;
        }

        // GET: api/Consistency
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Consistency>>> GetConsistencies()
        {
            return await _context.Consistencies.ToListAsync();
        }

        // GET: api/Consistency/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Consistency>> GetConsistency(int id)
        {
            var consistency = await _context.Consistencies.FindAsync(id);

            if (consistency == null)
            {
                return NotFound();
            }

            return consistency;
        }

        // PUT: api/Consistency/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutConsistency(int id, Consistency consistency)
        {
            try
            {
                Validate(consistency);
            }
            catch (Exception e)
            {
                return ValidationProblem(e.Message);
            }
            
            if (id != consistency.Id)
            {
                return BadRequest();
            }

            _context.Entry(consistency).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ConsistencyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Consistency
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Consistency>> PostConsistency(Consistency consistency)
        {
            try
            {
                Validate(consistency);
            }
            catch (Exception e)
            {
                return ValidationProblem(e.Message);
            }
            
            _context.Consistencies.Add(consistency);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetConsistency", new { id = consistency.Id }, consistency);
        }

        protected void Validate(Consistency consistency)
        {
            bool existAlready = _context.Consistencies.Any(c => c.Name == consistency.Name);

            if (existAlready)
            {
                throw new Exception("Consistency name already taken"); 
            }
        }

        // DELETE: api/Consistency/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Consistency>> DeleteConsistency(int id)
        {
            var consistency = await _context.Consistencies.FindAsync(id);
            if (consistency == null)
            {
                return NotFound();
            }

            _context.Consistencies.Remove(consistency);
            await _context.SaveChangesAsync();

            return consistency;
        }

        private bool ConsistencyExists(int id)
        {
            return _context.Consistencies.Any(e => e.Id == id);
        }
    }
}
