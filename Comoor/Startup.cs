using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Comoor.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;

namespace Comoor
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = GetConnectionString(
                Environment.GetEnvironmentVariable("DATABASE_URL")
            );
            services.AddEntityFrameworkNpgsql().AddDbContext<ComoorContext>(
                opt => opt.UseNpgsql(connectionString)
            );
            services.AddCors();
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "Comoor API", Version = "v1"});
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                RunMigrations(app);
            }
            
            
            app.UseHttpsRedirection();
            app.UseCors(
                options => options.WithOrigins(
                        "http://saycheese.docker.test",
                        "https://sprint-8-front-saycheese.herokuapp.com",
                        "https://saycheeses.herokuapp.com")
                    .AllowAnyMethod()
                    .AllowAnyHeader()
            );

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Comoor API V1");
                c.RoutePrefix = string.Empty;
            });
        }

        private static void RunMigrations(IApplicationBuilder app)
        {
            var context = app.ApplicationServices.GetService<ComoorContext>();
            context.Database.Migrate(); 
        }

        private static string GetConnectionString(string databaseUrl)
        {
            var isUrl = Uri.TryCreate(databaseUrl, UriKind.Absolute, out var url);
            if (!isUrl) return databaseUrl;
            var connectionUrl =
                $"Server={url.Host};" +
                $"User ID={url.UserInfo.Split(':')[0]};" +
                $"Password={url.UserInfo.Split(':')[1]};" +
                $"Database={url.LocalPath.Substring(1)};" +
                "Pooling=true;Integrated Security=true;";
            return connectionUrl;
        }
    }
}
