﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Comoor.Migrations
{
    public partial class RemoveUrole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_URoles_UroleId",
                table: "Users");

            migrationBuilder.DropTable(
                name: "URoles");

            migrationBuilder.DropIndex(
                name: "IX_Users_UroleId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "UroleId",
                table: "Users");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UroleId",
                table: "Users",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "URoles",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_URoles", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Users_UroleId",
                table: "Users",
                column: "UroleId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_URoles_UroleId",
                table: "Users",
                column: "UroleId",
                principalTable: "URoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
