﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Comoor.Migrations
{
    public partial class RemoveChesseType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_CheeseTypes_CheeseTypeId",
                table: "Products");

            migrationBuilder.DropTable(
                name: "CheeseTypes");

            migrationBuilder.DropIndex(
                name: "IX_Products_CheeseTypeId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "CheeseTypeId",
                table: "Products");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CheeseTypeId",
                table: "Products",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CheeseTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Affiliation = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CheeseTypes", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Products_CheeseTypeId",
                table: "Products",
                column: "CheeseTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_CheeseTypes_CheeseTypeId",
                table: "Products",
                column: "CheeseTypeId",
                principalTable: "CheeseTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
