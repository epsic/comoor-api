#!/bin/bash
MODEL=$1
docker-compose run tools \
dotnet aspnet-codegenerator controller -name ${MODEL}Controller -async -api -m ${MODEL} -dc ComoorContext -outDir Controllers
