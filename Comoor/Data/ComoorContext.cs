using System;
using Comoor.Models;
using Microsoft.EntityFrameworkCore;

namespace Comoor.Data
{
    public class ComoorContext: DbContext
    {
        public ComoorContext(DbContextOptions<ComoorContext> options)
            : base(options)
        {

        }
        public DbSet<AnimalOrigin> AnimalOrigins { get; set; }
        public DbSet<Consistency> Consistencies { get; set; }
        public DbSet<Origin> Origins { get; set; }
        public DbSet<Smell> Smells { get; set; }
        public DbSet<Stock> Stocks { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Comoor.Models.Cart> Cart { get; set; }
    }
}
