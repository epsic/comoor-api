using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Comoor;
using Comoor.Models;
using Newtonsoft.Json;
using Xunit;
using Xunit.Abstractions;
using static Saycheese.IntegrationTests.Data.Factory;


namespace Saycheese.IntegrationTests
{
    public class BasicTests
        : IClassFixture<SayCheeseAppFactory<Startup>>
    {
        private readonly ITestOutputHelper _testOutputHelper;
        private static HttpClient _client;

        public BasicTests(SayCheeseAppFactory<Startup> factory, ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task Get_EndpointReturnSuccess()
        {
            // Act
            var response = await _client.GetAsync("/api/AnimalOrigin");

            // Assert
            response.EnsureSuccessStatusCode();
            Assert.Equal("application/json; charset=utf-8",
                response.Content.Headers.ContentType.ToString());

            var animalOriginJson = await response.Content.ReadAsStringAsync();
            var deserializedResponse = JsonConvert.DeserializeObject<List<AnimalOrigin>>(animalOriginJson);
            Assert.Equal("Cow", deserializedResponse[0].Animal);
        }
    }
}