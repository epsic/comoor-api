using Comoor.Data;
using Saycheese.IntegrationTests.Data.Seeds;

namespace Saycheese.IntegrationTests.Data
{
    public static class Seed
    {
        public static void SeedDbForTests(ComoorContext db)
        {
            db.AnimalOrigins.AddRange((AnimalOriginSeed.GetSeeding()));
            db.SaveChanges();
        }

        public static void ReinitializeDbForTests(ComoorContext db)
        {
            db.AnimalOrigins.RemoveRange(db.AnimalOrigins);
            SeedDbForTests(db);
        }
    }
}