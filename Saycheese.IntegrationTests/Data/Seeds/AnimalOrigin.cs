using System.Collections.Generic;
using Comoor.Models;

namespace Saycheese.IntegrationTests.Data.Seeds
{
    public static class AnimalOriginSeed
    {
        public static IEnumerable<AnimalOrigin> GetSeeding()
        {
            return new List<AnimalOrigin>()
            {
                new AnimalOrigin() {Animal = "Cow"},
            };
        }
    }
}