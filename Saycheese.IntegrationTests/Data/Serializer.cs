using System;
using System.Collections.Generic;
using Comoor.Models;
using Microsoft.VisualStudio.Web.CodeGenerators.Mvc;
using Newtonsoft.Json;

namespace Saycheese.IntegrationTests.Data
{
    public class Serializer
    {
        public static T Deserializer<T>(T type, string value)
        {
            return JsonConvert.DeserializeObject<T>(value);
        }
    }
}